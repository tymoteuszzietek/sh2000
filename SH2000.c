/*
 * SH2000.c
 *
 * Created: 2018-05-26 17:14:19 PM
 *  Author: tymo
 */ 
#include "SH2000.h"

#include "lib/lcd.h"
#include "lib/Timer.c"
#include "lib/ADC.c"
#include "lib/menu.c"



#define key_up      4  //go up
#define key_down    2  //go down
#define key_left    8  //go to parent menu
#define key_right   1  //choose

const char  menu_string_0[] PROGMEM = "Timer ST 3000";
const char  menu_string_1[] PROGMEM = "[START]";
const char  menu_string_2[] PROGMEM = "[REVIEW]";
const char  menu_string_3[] PROGMEM = "[SETTINGS]";
const char  menu_string_4[] PROGMEM = "[SENSITIVE]";
const char  menu_string_5[] PROGMEM = "[DELAY]";
const char  menu_string_6[] PROGMEM = "[ECHO]";
const char  menu_string_7[] PROGMEM = "[BACKLIGHT LCD] ";
const char  menu_string_8[] PROGMEM = "[SAVE]";


const menu_item menu[] = {
	// U, D, O, E, I
	{{ 1, 1, 1, 0, 0}, NULL, menu_string_0},
	{{ 3, 2, 9, 1, 1}, NULL, menu_string_1},
	{{ 1, 3, 6, 1, 2}, NULL, menu_string_2},
	{{ 2, 1, 4, 1, 3}, NULL, menu_string_3},
	{{ 5, 14, 7, 3, 4}, NULL, menu_string_4},
	{{ 10, 4, 8, 3, 5}, NULL, menu_string_5},
	{{ 6, 6, 2, 2, 6}, reviewShots, NULL},
	{{ 7, 7, 7, 4, 7}, setSensitivity, NULL},
	{{ 8, 8, 5, 5, 8}, setDelay, NULL},
	{{ 9, 9, 9, 9, 9}, measureShotsTime, NULL},
	{{ 12, 5, 11, 3, 10}, NULL, menu_string_6},
	{{ 11, 11, 10, 10, 11}, setEchoProtect, NULL},
	{{ 14, 10, 13, 3, 12}, NULL, menu_string_7},
	{{ 13, 13, 12, 12, 13}, setBacklight, NULL},
	{{ 4, 12, 15, 3, 14}, NULL, menu_string_8},
	{{ 15, 15, 15, 14, 15}, saveSetingEvent, NULL}
};

const int shotLimit = SHOTLIMIT;
uint32_t shotTimes[SHOTLIMIT];
int threshold = MAX_THRESHOLD;

uint8_t currentShot = 0;
uint8_t reviewShot = 0;

uint8_t oldButtons;
uint8_t buttons;
const setting default_settings = {2,49,12};
setting settings;

int backlight = 1;
int saveSetingEventFlag = 0;

// Save data to EEPROM
void saveSettings(void) {
	eeprom_write_block(&settings, &eem_settings, sizeof(setting));
}

// Load data from EEPROM
void loadSettings(void) {
	int error =0;
	eeprom_read_block(&settings, &eem_settings, sizeof(setting));
	if(settings.sensitivity<0 || settings.sensitivity>35) error=1;
	if(settings.sampleWindow<0 || settings.sampleWindow>109) error=1;
	if(settings.delayTime<0 || settings.delayTime>12) error=1;
	if(error)
	{
	settings = default_settings;
	saveSettings();
	}
}

void debounce()
{
	uint32_t start_millis = millis();
	while( DEBOUNCE_TIME + start_millis > millis()){};
}

void beep(uint16_t time, uint16_t relase)
{
	uint32_t beep_start_millis = millis();
	PORTD &= ~(1<<BEEPER_PIN);
	PORTD |= (1<<BEEPER_PIN2);
	while( beep_start_millis + time > millis()){};
	PORTD |= (1<<BEEPER_PIN);
	PORTD &= ~(1<<BEEPER_PIN2);
	if(relase)
	{
		while( beep_start_millis + relase > millis()){};
	}
}

int randint(int min, int max)
{
	return min + rand() % (max+1 - min);
}

void startDelay(){
	
	uint32_t delay_start_millis = millis();
	
	if (settings.delayTime > 11) {
		uint16_t rTime = randint(2000,6001);
		while( delay_start_millis + rTime > millis()){};
	}
	else if (settings.delayTime == 11) {
		uint16_t rTime = randint(1000,4001);
		while( delay_start_millis + rTime > millis()){};
	}
	else {
		while( delay_start_millis + (settings.delayTime * 1000) > millis()){};
	}
}


void startTimer(){
	TIMER_reset();
	 for (int c = 0; c < currentShot; c++){ 
		 shotTimes[c] = 0;
	 }
	 currentShot = 0;
	 LCD_clear();
	 LCD_string("Wait for it...  ");
	 startDelay();
	 LCD_setCursor(0,0);

	 LCD_string_P(PSTR("      GO!!      "));

	 LCD_setCursor(0,1);
	 LCD_string_P(PSTR("Last:           "));
	 beep(BEEPER_TIME_MS,BEEPER_TIME_RELASE_MS);
	 TIMER_start();
	 
}

int sampleSound() {
	unsigned long startMillis = millis();

	int peakToPeak = 0; 
	int sample = 0;
	int signalMax = 0;
	int signalMin = 1024;
	
	while (millis() - startMillis < settings.sampleWindow)
	{
		sample = ADC_read();
		if (sample < 1024)
		{
			if (sample > signalMax)
			{
				signalMax = sample; 
			}
			else if (sample < signalMin)
			{
				signalMin = sample; 
			}
		}
	}
	peakToPeak = signalMax - signalMin;
	return(peakToPeak);
}

void recordShot(int sampleSoundValue){

	shotTimes[currentShot] = TIMER_value() + BEEPER_TIME_MS + BEEPER_TIME_RELASE_MS;
	LCD_setCursor(6,1);
	LCD_MTimeLcdPrint(shotTimes[currentShot],7);
	currentShot += 1;
	if (currentShot == shotLimit){
		stopTimer();
	}
	 
}

void listenForShots(){
	int sampleSoundValue = sampleSound();
	if(sampleSoundValue >= threshold){
		recordShot(sampleSoundValue);
	}
}

void stopTimer() {
	TIMER_stop();
	LCD_setCursor(0,0);
	LCD_string_P(PSTR("      STOP      "));
	beep(100,400);
	beep(100,0);
	current_menu = 2;
	menu_event = E_IDLE;
}

void measureShotsTime(uint8_t event)
{
	if (!TIMER_isRunning()){
		startTimer();
	}
}

void reviewShots(uint8_t event)
{
	switch (event)
	{
		case E_UP:
		if(currentShot == 0 || reviewShot == currentShot - 1){
			reviewShot = 0;
		}
		else {
			reviewShot++;
		}
		break;
		
		case E_DOWN:
		if (currentShot == 0){
			reviewShot = 0;
		}
		else if(reviewShot == 0){
			reviewShot = currentShot - 1;
		}
		else {
			reviewShot--;
		}
		break;
		
		default:
		if (currentShot > 0)
		{
			reviewShot = currentShot - 1;
		}
		break;	
	}
	
	LCD_clear();
	LCD_setCursor(0,0);
	LCD_string_P(PSTR("Shot #"));
	itoa(reviewShot+1,tempStr,10);
	LCD_string(tempStr);

	if (reviewShot+1 > 1)
	{
		LCD_setCursor(9,0);
		LCD_string_P(PSTR(" Split "));
	}
	LCD_setCursor(0,1);
	LCD_MTimeLcdPrint(shotTimes[reviewShot], 7);
	if (reviewShot+1 > 1)
	{
		LCD_string_P(PSTR(" "));
		LCD_MTimeLcdPrint(shotTimes[reviewShot]-shotTimes[reviewShot-1],5);
	}
	

}

void setEchoProtect(uint8_t event){
	switch (event)
	{
		case E_UP:
			  if(settings.sampleWindow == 109){
				  settings.sampleWindow = 9;
			  }
			  else {
				  settings.sampleWindow += 10;
			  }
		break;
		case E_DOWN:
			 if(settings.sampleWindow == 9){
				 settings.sampleWindow = 109;
			 }
			 else {
				 settings.sampleWindow -= 10;
			 }
		break;
	}

	LCD_clear();
	LCD_setCursor(0,0);
	LCD_string_P(PSTR("Echo            "));
	LCD_setCursor(0,1);
	LCD_string_P(PSTR("   ms           "));
	LCD_setCursor(0,1);
	itoa(settings.sampleWindow,tempStr,10);
	LCD_string(tempStr);
}void saveSetingEvent(uint8_t event){
	LCD_clear();
	LCD_setCursor(0,0);
	LCD_string_P(PSTR("Save?           "));
	switch (event)
	{
		case E_UP:
		if(saveSetingEventFlag == 1){
			saveSetingEventFlag = 0;
		}
		else {
			saveSetingEventFlag = 1;
		}
		break;
		case E_DOWN:
		if(saveSetingEventFlag == 0){
			saveSetingEventFlag = 1;
		}
		else {
			saveSetingEventFlag = 0;
		}
		break;				case E_OK:
		if(saveSetingEventFlag == 1){
			saveSettings();
			LCD_setCursor(0,0);
			LCD_string_P(PSTR("Settings Saved!!"));
			saveSetingEventFlag = 0;
		}
		break;
	}

	LCD_setCursor(0,1);
	if(saveSetingEventFlag)
	{
		LCD_string_P(PSTR("YES             "));
	}else
	{
		LCD_string_P(PSTR("NO              "));
	}
}
void setBacklight(uint8_t event){
	switch (event)
	{
		case E_UP:
		if(backlight == 1){
			backlight = 0;
		}
		else {
			backlight = 1;
		}
		break;
		case E_DOWN:
		if(backlight == 0){
			backlight = 1;
		}
		else {
			backlight = 0;
		}
		break;
	}

	LCD_clear();
	LCD_setCursor(0,0);
	LCD_string_P(PSTR("Backlight  LCD  "));
	LCD_setCursor(0,1);
	if(backlight)
	{
		LCD_Backlight(backlight);
		LCD_string_P(PSTR("ON              "));
	}else
	{
		LCD_Backlight(backlight);
		LCD_string_P(PSTR("OFF             "));
	}
}

void sensToThreshold(){
	threshold = MAX_THRESHOLD-(25*settings.sensitivity);
}

void setSensitivity(uint8_t event){
	switch (event)
	{
		case E_UP:
			if(settings.sensitivity == 35){
				settings.sensitivity = 0;
			}
			else {
				settings.sensitivity++;
			}
			sensToThreshold();
		break;
		case E_DOWN:
			if(settings.sensitivity == 0){
				settings.sensitivity = 35;
			}
			else {
				settings.sensitivity--;
			}
			sensToThreshold();
		break;
	}

	LCD_clear();
	LCD_setCursor(0,0);
	LCD_string_P(PSTR("Sensitivity     "));
	LCD_setCursor(0,1);
	LCD_string_P(PSTR("   level        "));
	LCD_setCursor(0,1);
	itoa(settings.sensitivity,tempStr,10);
	LCD_string(tempStr);
}

void setDelay(uint8_t event){

	switch (event)
	{
		case E_UP:
		 if(settings.delayTime == 12){
			 settings.delayTime = 0;
		 }
		 else {
			 settings.delayTime++;
		 }
		break;
		case E_DOWN:
		if(settings.delayTime == 0) {
			settings.delayTime = 12;
		}
		else {
			settings.delayTime--;
		}
		break;
	}
	
	LCD_clear();
	LCD_setCursor(0,0);
	LCD_string_P(PSTR("Start Delay     "));
	LCD_setCursor(0,1);
	if (settings.delayTime > 11) {
		LCD_string_P(PSTR("Random 2-6s     "));
	}
	else if (settings.delayTime == 11) {
		LCD_string_P(PSTR("Random 1-4s     "));
	}
	else {
		LCD_string_P(PSTR("   s            "));
		LCD_setCursor(0,1);
		itoa(settings.delayTime,tempStr,10);
		LCD_string(tempStr);
	}
}

uint8_t buttonEvents(uint8_t button)
{
	switch (button)
	{
		case key_up:
		return E_UP;
		break;
		
		case key_down:
		return E_DOWN;
		break;
		
		case key_left:
		return E_EXIT;
		break;
		
		case key_right:
		return E_OK;
		break;
		
		default:
		return E_IDLE;
		break;
		
	}
}

int main(void)
{
	ADC_init();
	TIMER_init();
	LCD_init();
	BUTTON_init();
	loadSettings();
	sensToThreshold();
	beep(100,400);
	beep(100,400);
	LCD_Backlight(backlight);
	change_menu();
	
	oldButtons = BUTTON_read();
    while(1)
    {
		uint8_t newButtons = BUTTON_read();
		buttons = newButtons & ~oldButtons;
		oldButtons = newButtons;
		
		if (buttons){
			if(TIMER_isRunning())
			{
				stopTimer();
			}
			else
			{
				debounce();
				menu_event = buttonEvents(buttons);
				change_menu();
			}
		}
		
		if(TIMER_isRunning()){
			listenForShots();
		}
    }
}