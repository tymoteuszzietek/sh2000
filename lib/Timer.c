/*
 * Timer.c
 *
 * Created: 2018-05-22 21:47:56 PM
 *  Author: tymo
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>

#define clockCyclesPerMicrosecond 8
#define MICROSECONDS_PER_TIMER0_OVERFLOW 1000
#define MILLIS_INC (MICROSECONDS_PER_TIMER0_OVERFLOW / 1000)
#define FRACT_INC ((MICROSECONDS_PER_TIMER0_OVERFLOW % 1000) >> 3)
#define FRACT_MAX (1000 >> 3)

volatile uint32_t timer0_millis = 0;
volatile unsigned char timer0_fract = 0;
volatile unsigned long timer0_overflow_count = 0;
uint32_t TIMER_start_millis = 0;
uint32_t TIMER_stop_millis = 0;
enum TimerState { RESET, RUNNING, STOPPED };
enum TimerState TIMER_State = RESET;

ISR(TIMER0_OVF_vect) {
	uint32_t m = timer0_millis;
	unsigned char f = timer0_fract;
	TCNT0 = 0x83;
	m += MILLIS_INC;
	f += FRACT_INC;
	if (f >= FRACT_MAX) {
		f -= FRACT_MAX;
		m += 1;
	}
	
	timer0_fract = f;
	timer0_millis = m;
	timer0_overflow_count++;
}

unsigned long millis() {
	uint32_t m;
	uint8_t oldSREG = SREG;
	
	cli();
	m = timer0_millis;
	SREG = oldSREG;
	
	return m;
}

void TIMER_init(void) {
	cli();
	TCCR0 |= (1<<CS00);
	TCCR0 |= (1<<CS01);
	TIMSK |= (1<<TOIE0);
	sei();
}

enum TimerState TIMER_state() {
	 return TIMER_State;
}

bool TIMER_isRunning(){
	return (TIMER_State == RUNNING);
}

void TIMER_start() {
	if (TIMER_State == RESET || TIMER_State == STOPPED)
	{
		TIMER_State = RUNNING;
		uint32_t t = timer0_millis;
		TIMER_start_millis += t - TIMER_stop_millis;
		TIMER_stop_millis = t;
	}
}

void TIMER_stop(){
	if (TIMER_State == RUNNING){
		TIMER_State = STOPPED;
		TIMER_stop_millis = timer0_millis;
	}
}

uint32_t TIMER_value(){
	 if (TIMER_State == RUNNING){ 
		 TIMER_stop_millis = timer0_millis;
	 }
	 return TIMER_stop_millis - TIMER_start_millis;
}
 
void TIMER_reset(){
	TIMER_State = RESET;
	TIMER_start_millis = TIMER_stop_millis = 0;
}