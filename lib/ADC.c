/*
 * ADC.c
 *
 * Created: 2018-05-26 17:14:19 PM
 *  Author: tymo
 */ 
void ADC_init(void)
{
	ADCSR = (1<<ADEN)|(1<<ADPS1)|(1<<ADPS2);
}

int ADC_read()
{
	uint8_t low, high;
	ADCSR |= (1<<ADSC);
	while (bit_is_set(ADCSR, ADSC));
	low  = ADCL;
	high = ADCH;
	return (high << 8) | low;
}