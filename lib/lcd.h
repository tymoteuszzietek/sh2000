#ifndef LiquidCrystal_h
#define LiquidCrystal_h

#include <stdlib.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#define EN 0x02
#define RS 0x01
#define LCD_BACKLIGHT PD5
//all pin are connected to portB
//#define D4 PORTB2
//#define D5 PORTB3	
//#define D6 PORTB4	
//#define D7 PORTB5	

char str[16];


void LCD_cmd(unsigned char cmd);
void LCD_init();
void LCD_setCursor(unsigned char row, unsigned char column);
void LCD_clear();
void LCD_cmd(unsigned char cmd)
{
	//select portB for lcd operations
	char data= ((cmd & 0xF0)>>2);
	PORTB = data;
	PORTB = data | EN;
	_delay_ms(1);
	PORTB = data;
	_delay_ms(1);
	PORTB &= 0xF0>>2;
	
	data= (((cmd<<4) & 0xF0)>>2);
	PORTB = data;
	PORTB = data | EN;
	_delay_ms(1);
	PORTB = data;
	_delay_ms(1);
	return;
}
void LCD_init()
{
	DDRB = 0x3F;		//select portB for lcd operations
	PORTB &=(0x00)>>2;
	LCD_cmd(0x33);       // configuring LCD as 2 line 5x7 matrix in 4-bit mode
	_delay_ms(40);
	LCD_cmd(0x32);       // configuring LCD as 2 line 5x7 matrix in 4-bit mode
	_delay_ms(40);
	LCD_cmd(0x28);       // Display on, Cursor blinking
	_delay_ms(10);
	LCD_cmd(0x0c);       // <span class="IL_AD" id="IL_AD7">Clear Display</span> Screen
	_delay_ms(10);
	LCD_cmd(0x01);       // Increment Cursor (Right side)
	_delay_ms(10);
	LCD_cmd(0x06);       // Increment Cursor (Right side)
	_delay_ms(10);
}

void LCD_data(unsigned char data1){
	DDRB = 0x3F;	//select portB for lcd operations
	PORTB=0;
	char data= ((data1 & 0xF0)>>2) | RS;
	PORTB = data ;
	PORTB = data | EN;
	_delay_ms(1);
	PORTB = data;
	_delay_ms(1);
	data= (((data1<<4) & 0xF0)>>2) | RS;
	PORTB = data ;
	PORTB = data | EN ;
	_delay_ms(1);
	PORTB = data;
	_delay_ms(1);
	return;
}
void LCD_string(const char* data){
/*	unsigned char byteToWrite = sizeof(data);*/
	while(*data)
	LCD_data(*data++);
}

void LCD_string_P(PGM_P data){
	strcpy_P(str,data);
	LCD_string(str);
}

void LCD_setCursor( unsigned char column, unsigned char row){
	unsigned char cmd=0;
	switch (row){
		case 0: cmd= 0x80 + column; break;
		case 1: cmd = 0xc0 + column; break;
		case 2: cmd = 0x94 + column; break;
		case 3: cmd = 0xd4 +column;
	}
	LCD_cmd(cmd);
	_delay_ms(1);
}
void LCD_clear(){
	LCD_cmd(0x01);
}



/////////////////////////////////////////////////////////////
// Helper - 2 digits
/////////////////////////////////////////////////////////////

void LCD_lcd2digits(uint32_t x)
{
	if (x < 10) LCD_string("0");
	itoa(x,str,10);
	LCD_string(str);
}

/////////////////////////////////////////////////////////////
// helper - 3 digits
/////////////////////////////////////////////////////////////
void LCD_lcd3digits(uint32_t x)
{
	if (x < 100) LCD_string("0");
	if (x < 10) LCD_string("0");
	itoa(x,str,10);
	LCD_string(str);
}

/////////////////////////////////////////////////////////////
// helper - 4 digits
/////////////////////////////////////////////////////////////

void LCD_lcd4digits(uint32_t x)
{
	if (x < 1000) LCD_string("0");
	if (x < 100) LCD_string("0");
	if (x < 10) LCD_string("0");
	itoa(x,str,10);
	LCD_string(str);
}

/////////////////////////////////////////////////////////////
// Print time to an LCD screen
/////////////////////////////////////////////////////////////

void LCD_MTimeLcdPrint(uint32_t t, int digits)
{
	uint32_t x;
	if (digits >= 8)
	{
		// HOURS
		x = t/ 3600000UL;
		t -= x * 3600000UL;
		LCD_lcd2digits(x);
		LCD_string(":");
	}
	if (digits >= 6) {
		// MINUTES
		x = t/ 60000UL;
		t -= x * 60000UL;
		LCD_lcd2digits(x);
		LCD_string(":");
	}
	if (digits >= 4){
		// SECONDS
		x = t/1000UL;
		t -= x * 1000UL;
		LCD_lcd2digits(x);
	}
	if (digits == 9 || digits == 7 || digits == 5)
	{
		// THOUSANDTHS
		LCD_string(".");
		//x = (t+5)/10L;  // rounded hundredths?
		LCD_lcd3digits(t);
		} else if (digits >= 2){
		// ROUNDED HUNDREDTHS
		LCD_string(".");
		x = (t+5)/10L;  // rounded hundredths?
		LCD_lcd2digits(t);
	}
}

void LCD_Backlight(uint8_t x)
{
	if (x)
	{
		PORTD |= (1<<LCD_BACKLIGHT);
	}
	else
	{
		PORTD &= ~(1<<LCD_BACKLIGHT);
	}
	
}

#endif