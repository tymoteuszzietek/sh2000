#include <string.h>
#define E_UP 0
#define E_DOWN 1
#define E_OK 2
#define E_EXIT 3
#define E_IDLE 4

volatile uint8_t	current_menu = 0;
volatile uint8_t	menu_event = E_IDLE;

typedef struct
{
	uint8_t next_state[5];		//przej?cia do nast?pnych stan�w
	void (*callback)(uint8_t event);	//funkcja zwrotna
	const char* first_line;		//tekst dla 1. linii LCD
	
} menu_item;

const menu_item menu[];

void change_menu()
{
	current_menu = menu[current_menu].next_state[menu_event];
	if (menu[current_menu].callback)
	{	
		menu[current_menu].callback(menu_event);
	}
	else
	{
		LCD_clear();
		LCD_string_P(menu[current_menu].first_line);
	}
	menu_event = E_IDLE;
}