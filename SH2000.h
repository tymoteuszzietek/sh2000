/*
 * SH2000.h
 *
 * Created: 2018-05-26 17:14:19 PM
 *  Author: tymo
 */ 
#ifndef SH2000_H_  
#define SH2000_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <avr/eeprom.h>

#define F_CPU 8000000UL
#define SHOTLIMIT 100
#define LCD_LINES 2
#define BEEPER_PIN2 PD6
#define BEEPER_PIN PD7
#define BEEPER_TIME_MS 300
#define BEEPER_TIME_RELASE_MS 50
#define MAX_THRESHOLD 950
#define DEBOUNCE_TIME 250

typedef struct
{
	int sensitivity;
	int sampleWindow;
	int delayTime;
} setting;

int sensitivity;
int sampleWindow;
int delayTime;
char tempStr[16];

setting EEMEM eem_settings;

void beep(uint16_t time, uint16_t relase);
int randint(int min, int max);
void startDelay();
void startTimer();
int sampleSound();
void recordShot();
void listenForShots();
void stopTimer();
uint8_t buttonEvents(uint8_t button);


void measureShotsTime(uint8_t event);
void reviewShots(uint8_t event);
void setEchoProtect(uint8_t event);
void setSensitivity(uint8_t event);
void setDelay(uint8_t event);
void setBacklight(uint8_t event);
void saveSetingEvent(uint8_t event);

void BUTTON_init()
{
	DDRD =  0b11100000;
	PORTD = 0b00011111;

}

uint8_t BUTTON_read()
{
	return PIND & 0b00011111;
}

int BUTTON_UP(uint8_t buttons)
{
	return buttons & (1<<PD2);
}

int BUTTON_DOWN(uint8_t buttons)
{
	return buttons & (1<<PD1);
}

int BUTTON_LEFT(uint8_t buttons)
{
	return buttons & (1<<PD3);
}

int BUTTON_RIGHT(uint8_t buttons)
{
	return buttons & (1<<PD0);
}


#endif 